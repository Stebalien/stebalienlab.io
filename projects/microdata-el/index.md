---
title: microdata.el
description: An Emacs package for extracting microdata from HTML and emails.
---

[microdata.el](https://github.com/Stebalien/microdata.el) is an Emacs package for extracting and acting on microdata from HTML and emails. Personally, I use this package to open Discourse and GitHub PR/issue notifications from [notmuch](https://notmuchmail.org/).

This package is not currently available on MELPA.

# Configuration

You'll need to install this package from source with your favorite Emacs package manager.

```lisp
(use-package microdata
  :vc (:url "https://github.com/Stebalien/microdata.el"))
```

Personally, I use this package with notmuch and distribute a notmuch integration library along with it. However, there's no reason it wouldn't work with [mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) or [Gnus](https://www.gnus.org/).

For example, the following will open the email's default action with `C-c C-c`:

```lisp
(use-package notmuch-microdata
  :bind
  (:map notmuch-show-mode-map
  "C-c C-c" 'notmuch-microdata-show-action-view
  :map notmuch-show-mode-map
  "C-c C-c" 'notmuch-microdata-search-action-view))
```

# API

The core [microdata.el](https://github.com/Stebalien/microdata.el/blob/master/microdata.el) library provides 4 key functions:

1. `(microdata-from-html)` for extracting microdata from a buffer containing HTML.
2. `(microdata-from-email)` for extracting microdata from a buffer containing a raw email message.
3. `(microdata-email-actions)` for extracting the list of "actions" that can be performed on an email. This list has the form `(("ActionName" . ((url . "https://some.url") (type . "SomeActionType"))))`.
4. `(microdata-email-actions-by-type TYPE)` for extracting the list of "actions" with the given TYPE. This list has the form `'(("ActionName" . "https://some.url"))`.

With emails, you'll usually want to call `(microdata-email-actions-by-type "ViewAction")`.
