---
title: Platter
index: true
description: A simple file server for direct file transfers.
---

Platter is a simple file server for direct file transfers. It's basically
[woof][woof] but with a GUI and some useful features.

Screenshot
----------

![Screenshot](./static/screenshot.png)

Features
--------

* GUI (Qt). While I generally avoid GUIs, qrcodes don't display very well on
  terminals.
* Displays the download progress of connected clients.
* Displays a QRCode (for transfering files to phones).
* Can bundle multiple files/directories into Zip archives.
* Can add and remove files/archives at runtime.
* Files/archives may be added at runtime.
* Single instance (additional invocations add files to the original instance)

Source
--------

[GitHub](https://github.com/Stebalien/Platter)

[woof]: http://www.home.unix-ag.org/simon/woof.html
