---
title: Stebalien's Corner Of The Net
about:
  name: Steven Allen
  email: steven@stebalien.com
  photo: static/img/logo.svg
  nicknames:
    - steb
    - stebalien
  key:
    fingerprint: "327B 20CE 21EA 68CF A774  8675 7C92 3221 5899 410C"
    url: static/key.pgp
  also:
    - "GitHub" : https://github.com/Stebalien
    - "Mastodon": https://mastodon.social/@stebalien
    - "Bluesky": https://bsky.app/profile/stebalien.com
---

I'm Stebalien (a.k.a. Steven) and this is my little corner of the net.

I've been working on decentralized systems and protocols since 2017; currently at [FilOz](http://filoz.org/), a member of the [Protocol Labs](https://protocol.ai) network. Previously, I built the [Filecoin Virtual Machine](https://fvm.filecoin.io/) and before that I maintained [kubo](https://ipfs.io) (go-ipfs) and [go-libp2p](https://libp2p.io).

When I'm not working, I hack on my [Emacs](https://www.gnu.org/software/emacs/) config.

If you're here because I've been ignoring some PR/issue, please send me an email. If I don't respond, feel free to bug me once every few days, I won't be offended.

---

Note: If you decide to read old posts on my blog, please check the date and keep in mind that I wrote everything before 2011 in while High School, and everything before 2017 while in University.
