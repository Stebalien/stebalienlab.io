---
title: "Packagekit with apturl"
date: "2010-04-28"
permalink: posts/2010/04/packagekit-with-apturl/
tags:
 - Tutorial
 - Script
---

Although I no longer use packagekit, I still have my apturl script so I
thought I would post it. This script will allow you to open apt://
scripts with packagekit. (Just save this to a file, mark it executable,
and tell your browser to open apt scripts with it).

    #!/bin/bash
    /usr/bin/gpk-install-package-name $(echo $* | sed -e 's/apt:\/\?\/\?//')

