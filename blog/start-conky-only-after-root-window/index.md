---
title: "Start conky only after the root window loads"
date: "2010-01-02"
permalink: posts/2010/01/start-conky-only-after-root-window/
tags:
 - Tutorial
 - Script
 - Linux
---

Every time I logged in, conky would start up before nautilus loaded the
desktop. This caused conky to load as a floating window that stayed on
top of all other windows. I have finally gotten around to fixing this
problem.

~~[Here](http://paste2.org/p/591516)~~ (gone, email if found) is a simple python
script that waits for nautilus to load the desktop before starting conky.

This script is probably very inefficient but it gets the job done.

