---
title: "Installing Ubuntu beta"
date: "2008-04-03"
permalink: posts/2008/04/installing-ubuntu-beta/
tags:
 - Linux
---

The wireless card continues to not work but I am installing anyway
(nothing to loose). I am hoping that the fix on
[launchpad](https://bugs.launchpad.net/ubuntu/+bug/139642) will work.

Edit: The battery monitor applet does work in GNOME.

