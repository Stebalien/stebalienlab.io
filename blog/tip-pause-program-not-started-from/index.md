---
title: "Tip: Pause a program not started from the command line"
date: "2008-04-29"
permalink: posts/2008/04/tip-pause-program-not-started-from/
tags:
 - Tip
 - Linux
---

This tip is a follow-up from the last one. To pause a program that was
not started from a terminal, simply go to the
[gnome-system-manager](apt:gnome-system-manager), right click, and
select stop. Select continue to resume a paused process.

