---
title: "10 Hour Fix"
date: "2008-03-09"
permalink: posts/2008/03/10-hour-fix/
tags:
 - Linux
---

Today I spent 10 hrs fixing my hostname.

 I had just bought the domain stebalien.com and was fiddling around with
my network settings with **network-admin** and found the domain box. I
typed in my new domain to see what the setting did. Nothing happened for
a while so I erased the setting and changed the hosts file back to its
origional state (without the domain name). Later, while I was using
firefox, my session crashed. I logged back in and received this error:

    (process:6725): Gtk-WARNING **: This process is currently running setuid or setgid.
    This is not a supported use of GTK+. You must create a helper
    program instead. For further details, see:

        http://www.gtk.org/setuid.html

    Refusing to initialize GTK+.

    (process:6729): Gtk-WARNING **: This process is currently running setuid or setgid.
    This is not a supported use of GTK+. You must create a helper
    program instead. For further details, see:

        http://www.gtk.org/setuid.html

    Refusing to initialize GTK+.

I was consequently logged out. This error had nothing to do with the
hostname so I assumed that one of my files had been corrupted. After
reinstalling half of my system (`sudo aptitude reinstall packages`) and
looking through all of my configuration files I finally logged in using
`startx`. I was immediately logged back out with a
`hostname internal error`. I spent another hour going over my host
configuration files (/etc/hosts, /etc/hostname, ...) and all of my DNS
configuration files. I finally logged back in using the **Failsafe
Terminal**, typed in `sudo network-admin` and re-added my domain name,
logged back out and in, and my session did not crash. I then proceeded
to launch firefox. The page did not load. I could ping, tracerout, and
use elinks (a text based web browser for the console) but I Firefox
would not load any website. I tinkered with my network-admin settings
for 15 minuets before I finally realized that **I had a backup** of my
network-admin settings. I restored my settings and proceeded to write
this blog post. I am still clueless as to what my problem was but am
happy that it was at least fixed.

