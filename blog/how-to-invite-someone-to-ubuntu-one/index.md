---
title: "How to invite someone to Ubuntu One"
date: "2009-05-24"
permalink: posts/2009/05/how-to-invite-someone-to-ubuntu-one/
tags:
 - Tutorial
 - Tip
 - Linux
---

For anyone with an Ubuntu One account, the following are instructions
for sending invites:


1.  Go to [My Files](https://ubuntuone.com/files/#path=/My%20Files) in
    the web interface.
2.  Create a new folder (to be shared) with the person that you will be
    inviting

3.  Go to the sharing tab (on the right).
4.  Share the folder with the person that you want to invite (The trick
    is that someone does not have to have an Ubuntu One account to
    accept a share).

[http://ubuntuone.com](http://ubuntuone.com/)



For anyone looking for an invite, just ask
@[bugabundo](http://identi.ca/notice/new?replyto=bugabundo) at Identi.ca

