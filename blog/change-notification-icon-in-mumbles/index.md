---
title: "Change the notification icon in Mumbles without creating a plugin."
date: "2008-11-17"
permalink: posts/2008/11/change-notification-icon-in-mumbles/
tags:
 - Tutorial
 - Linux
 - Mumbles
---

I have noticed that several people have asked how to change the
notification icon on mumbles without creating a new plugin.This is
actually very simple: use the generic DBus plugin with dbus-send. I
believe that this tutorial requires the SVN version but I may be
mistaken. First add a new section to your DBus plugin configuration
file. If you do not have this configuration file, copy the sample one
from the mumbles source tarball (found on the project's homepage). The
file is called `dbus.conf.sample`. Copy this file to
`~/.mumbles/dbus.conf`. The new section is as follows:


    [program-or-script-name]
    enabled = true
    interface = org.program-or-script-name.DBus
    path = /org/program-or-script-name/DBus
    signal = your-signal
    icon = your-icon.png



Put *your-icon.png* in the `~/.mumbles/plugins/icons/`
folder.*your-signal* is your name for the alert. None of these variables
are important but they must be consistent.



After doing the previous and restarting mumbles, run this command:

    dbus-send /org/program-or-script-name/DBus org.`*program-or-script-name*`.DBus.your-signal string:"Title" string:"Message"

Make sure that you replace all necessary variables. This will display a
mumbles popup with your custom icon, title, and message. You can also
specify a click action using `launch = action`.

