---
title: "Install fonts using nautilus-actions"
date: "2008-05-31"
---

[Nautilus-Actions](apt:nautilus-actions) installs custom entries in the
context menu in nautilus. I needed a simple way to install fonts so I
wrote a command for nautilus-actions.

Set "Path:" to `bash`

Set Parameters to
`-c "if zenity --question --text=\"Would you like to install the font system wide?\" --title=\"Install to System\"; then gksudo cp %M /usr/share/fonts/truetype/local/%m; else cp %M ~/.fonts/%m; fi; exit 0;"`.

If you do not want to install nautilus actions you can simply create a
Nautilus script.


The contents of the script should be:

    #!/bin/bash
    if zenity --question --text="Would you like to install the font system wide?" --title="Install to System"; then
         for $i in $NAUTILUS_SCRIPT_SELECTED_FILE_PATHS; do
              gksudo cp $i /usr/share/fonts/truetype/local/$(basename $i)
         done
    else
         for $i in $NAUTILUS_SCRIPT_SELECTED_FILE_PATHS; do
              cp $i ~/.fonts/$(basename $i)
         done
    fi
    exit 0



Save this script in '\~/.gnome2/nautilus-scripts' and make it executable
(`chmod u+x`).

