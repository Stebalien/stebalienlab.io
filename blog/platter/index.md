---
title: Platter
date: 2014-01-31
cc: projects/platter
---

Announcing [platter](projects/platter/), a file server for direct file
transfers. See the project page for details.

![Screenshot](projects/platter/static/screenshot.png)


