---
title: "Apt Repository Permissions"
date: "2009-08-06"
permalink: posts/2009/08/apt-repository-permissions/
---

I just posted a solution to
[this](http://brainstorm.ubuntu.com/item/20622/) idea but thought that I
should share it here.

Here is the problem: In order to get the latest features on Ubuntu,
people are adding a lot of PPAs. For now there hasn't, as far as I know,
been a case in which a PPA owner has uploaded a malicious package but
this is a possibility. Uploading an end user application, such as
shutter, with malicious code would be problematic but not devastating.
On the other hand, uploading a malicious sudo package would be
devastating. Here is my solution.



Different repositories would "own" packages:

1. Ownership would be set in a file such as /etc/apt/ownership/.list

2. A special system packages file would be created that would designate
system packages (sudo, pam etc...).



Apt repositories would have permissions:

0. Ultimate Trust: Update and Install packages from this repository
regardless of ownership including system packages

1. All: Update and Install new packages from this repository regardless
of ownership (except system owned packages).

2. Owned only: Update and install only owned packages.

3. No Updates: Install owned packages from this repository but do not
download updates from it.



Flags:

1. Warning: There would be a warning flag that a user could set on a
repository that would warn when packages are updated or installed from
that repository.

2. System: There would be a system flag that could be set on security
related packages (sudo, bash etc...) that would prevent all but
"Ultimate Trust" repositories from installing/updating them.

