---
title: "Make Gksu and Policykit red"
date: "2010-04-29"
permalink: posts/2010/04/make-gksu-and-policykit-red/
tags:
 - Tip
---

I was bored one day and decided to make my gksu(do) and policykit
dialogs red. The results are actually quite nice.

[![](./static/gksu.th.png)](./static/gksu.png)



Add this to the bottom of your gtkrc file:

    style "gksu" {
        bg[NORMAL] = "#770000"
        bg[ACTIVE] = "#550000"
        bg[PRELIGHT] = "#990000"
        bg[SELECTED] = "#550000"
        bg[INSENSITIVE] = "#220000"
    }

    widget "GksuuiDialog*" style "gksu"widget "PolkitGnomeAuthenticationDialog*" style "gksu"

