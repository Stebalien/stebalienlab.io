---
title: "2wire+iwl3945=Crash"
date: "2008-04-06"
permalink: posts/2008/04/2wireiwl3945crash/
tags:
 - Linux

---

It turns out that my wireless problem is in my router not my computer.
My 2wire router crashes when I try to connect to it using the `iwl3945`
driver. Ubuntu Gutsy used the `ipw3945` driver (now deprecated). That
driver worked. Otherwise, Hardy is cool.

