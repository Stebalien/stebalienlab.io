---
title: "Taboo: Can't read it now? Save it for later."
date: "2008-04-21"
permalink: posts/2008/04/taboo-can-read-it-now-read-it-for-later/
tags:
 - Review
 - Extension
---

[Taboo](https://addons.mozilla.org/en-us/firefox/addon/taboo/) is basically a
temporary bookmarks manager. If you have found an article that you find
interesting but it is too long you would normally have 3 options.

1. Bookmark it.

2. Leave the tab open and move on.

3. Close the tab and find it in your history.

All of these options have one major problem: Clutter. Bookmarks can
either be stored in a menu or the bookmarks toolbar. This is fine for
sites that are frequented often or are permanent references but
temporary bookmarks make the perminant bookmarks harder to find. A
separate temporary bookmarks folder is fine but it can be very hard to
find a specific bookmark mixed in with all of the other temporary
bookmarks. Leaving the bookmark in your tabbar makes sorting through
your tabs frustrating. The history is normaly erased after a set period
of time ind is a pain to look through.

Taboo solves these problems. If you can't remember the sites name, you
can look for the thumbnail of the site. If you can only remember when
you looked at the site you can view your taboos in calendar mode. If you
know that you added your taboo recently (within the last 10 or so) you
can use the menu attached to the "View Taboos" button as the list is
sorted by most recently added. Taboo is a beta so more features should
appear as time goes on.

Screenshots:

[![](./static/taboocal.th.png)](./static/taboocal.png)
[![](./static/taboogrid.th.png)](./static/taboogrid.png)
[![](./static/taboocalexpanded.th.png)](./static/taboocalexpanded.png)

