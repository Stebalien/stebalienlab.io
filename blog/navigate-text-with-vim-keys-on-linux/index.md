---
title: "Navigate text with vi(m) keys on linux"
date: "2009-06-05"
permalink: posts/2009/06/navigate-text-with-vim-keys-on-linux/
tags:
 - Tutorial
 - Linux
---

## Intro

Lifehacker recently [posted][lh] a AutoHotkey script for windows that allows
text navigation using letters instead of the arrow keys. In response to
[@wersdaluv][wersdaluv]'s post, I wrote a very simple script that allows users
to navigate text using the standard vim keys (hjkl) when the caps-lock key is
toggled. Feel free and add to my very basic script


## Steps

1.  Download my script (via copy and paste) from
    ~~[here](http://paste2.org/p/249823)~~. Lost (email if found).
2.  Save the script somewhere where you will not delete it and mark it
    as executable (`chmod u+x /path/to/script.sh`)
3.  Add the script to the startup programs with the argument `init`
    (i.e. `/path/to/script.sh init`). If you don't know how to add
    startup programs in your Desktop Environment, Google it.
4.  Assign F13 (the now remapped capslock key), as a hotkey in your
    window manager. Set the command to `'/path/to/script.sh toggle'`.
    Again, if you don't know how to add a hotkey, Google it.
5.  Now either log out and then in or run '`/path/to/script.sh init'` in
    order to remap the capslock key.
6.  Pressing the capslock key should now toggle navigation mode.



[lh]: http://lifehacker.com/5277383/use-caps-lock-for-hand+friendly-text-navigation
[wersdaluv]: http://identi.ca/wersdaluv
