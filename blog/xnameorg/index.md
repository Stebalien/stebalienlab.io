---
title: "Xname.org"
date: "2008-03-12"
permalink: posts/2008/02/xnameorg/
tags:
 - Tip
---

I just signed up with [1and1.com](http://www.1and1.com/)
and found that they do not offer marginally advanced DNS settings and
allow only five subdomains. To work around these limitations I signed up
with [Xname.org](http://www.xname.org/). They allow unfettered
customization of the DNS record and are free. I very much recommend them
(and thank them).

