---
title: "PCMan File Manager: 7z and rar archives"
date: "2008-05-25"
permalink: posts/2008/05/pcman-file-manager-7z-and-rar-archives/
tags:
 - Tutorial
 - Linux
 - software
---

By default, pcmanfm will not extract/create 7zip and Rar archives.
Extracting rar archives is already implemented but not enabled by
default while the ability to extract 7z archives is not implemented at
all. To enable the extraction of rar and 7zip archives and the creation
of 7zip archives open `src/ptk/ptk-file-archiver.c`:





Uncomment:

           {
               "application/x-rar",
               NULL,
               "unrar -o- e",
               ".rar", TRUE
           }

And add this below:

           {
               "application/x-7z-compressed",
               "7zr a -bd -y",
               "7zr x -bd -y",
               ".7z", TRUE
           }



Next, insert a comma, after the end bracket of the rar section.

The archive handlers section should endup looking like this:


    const ArchiveHandler handlers[]=
       {
           {
               "application/x-bzip-compressed-tar",
               "tar --bzip2 -cvf",
               "tar --bzip2 -xvf",
               ".tar.bz2", TRUE
           },
           {
               "application/x-compressed-tar",
               "tar -czvf",
               "tar -xzvf",
               ".tar.gz", TRUE
           },
           {
               "application/zip",
               "zip -r",
               "unzip",
               ".zip", TRUE
           },
           {
               "application/x-tar",
               "tar -cvf",
               "tar -xvf",
               ".tar", TRUE
           },
           {
               "application/x-rar",
               NULL,
               "unrar -o- e",
               ".rar", TRUE
           },
           {
               "application/x-7z-compressed",
               "7zr a -bd -y",
               "7zr x -bd -y",
               ".7z", TRUE
           }
       };

C0mpile with a standard `./configure; make; make install` or
`./configure; make; checkinstall`.

