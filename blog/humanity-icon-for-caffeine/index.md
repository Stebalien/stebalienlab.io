---
title: "Humanity Icon for Caffeine"
date: "2009-11-15"
permalink: posts/2009/11/humanity-icon-for-caffeine/
tags:
 - Tip
 - Review
 - Linux
---

For those who don't know, Caffeine is a small program for Linux that
lets a user prevent his or her computer from entering a power save
state. If a user wishes, he or she can even configure caffeine to
automatically inhibit power-saving when watching a flash movie, or
running VLC, Totem etc. For more information, visit its website
[here](http://pragmattica.wordpress.com/2009/10/21/caffeine-1-0-released/).



As a user of both Caffeine and the new Humanity icon theme (the default
icon theme in karmic), I made a very basic gray-scale version of the
Caffeine icon. You can download it
[here](http://gnome-look.org/content/show.php?content=115535).

