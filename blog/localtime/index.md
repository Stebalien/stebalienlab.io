---
title: Localtime
date: 2016-06-16
cc: projects/localtime
---

Localtime is a small, light-weight go daemon for keeping the timezone
up-to-date. It uses geoclue2 and systemd-timedated to do all the heavy lifting
so it's able to run with minimal privileges. See the
[project page](projects/localtime/) for more information.
