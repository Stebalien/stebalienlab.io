---
title: "'Pause' command in linux"
date: "2008-04-24"
permalink: posts/2008/04/command-in-linux/
tags:
 - Tip
 - Linux
---

Windows comes with a very useful `pause` command. Linux does not. This
can be problematic when running programs in the terminal from the GUI.
If a program runs in a terminal but did not originated in the terminal
(i.e. you double click on a file in nautilus and select run in terminal)
the terminal window closes immediately when the program finishes. This
makes it impossible to read the output from the terminal.


This function can be added to your \~/.bashrc file:

    function pause(){
       test $*  read -p "$*" || read -p "Press enter to continue..."
    }

I found this code on
[www.cyberciti.biz](http://www.cyberciti.biz/tips/linux-unix-pause-command.html)
but edited it to allow for no arguments.

