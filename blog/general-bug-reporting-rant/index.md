---
title: "General bug reporting rant"
date: 2008-09-19
---

I am starting to see this very often and find it very annoying: people
report a bug, later they find a preexisting report for the same bug,
they then mark that preexisting bug as a duplicate of their bug. I end
up getting lots of "this bug has been marked as a duplicate of bug x"
emails and this is VERY annoying. To prevent this people need to
rigorously check for a preexisting report. Second, when you find two
duplicate bugs, search the bug tracker for any more duplicates and mark
all of the newest duplicates as duplicates of the oldest bug. This
prevents chains of duplicates which in turn maximizes the efficiency of
the bug reporting and fixing process.

Example:

Chained:

```text
 report 1     ->    report 2     ->     report 3     ->     report 4
    |                  |                   |                   |
Commenters         Commenters           Commenters         All commenters
from report 1      from reports 1-2     from report 1-3
```



Non-chained:

```text
report 1 ----\
 commenters 1 \
report 2 ------> report 4
 commenters 2 /   all commenters
report 3 ----/
 commenters 3
```



In the chained version commenters move from report to report wasting
comments on bugs that will probably never be seen again after being
marked a duplicate. In the non-chained version commenters move directly
from the duplicate to the final (and oldest) bug report wasting as few
as comments possible on the duplicates.

