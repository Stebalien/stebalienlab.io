---
title: "Tip: Faster Ubuntu upgrade"
date: "2008-04-24"
permalink: posts/2008/04/tip-faster-ubuntu-upgrade/
tags:
 - Tip
 - Linux
---

I am currently upgrading to Ubuntu 8.04 and so far so good. For the past
releases I have upgraded from the standard server but that was very slow
(8 hour download).



Fix: Use another server.



My current download is almost complete and my download speed is
currently at 164kb/s (my max, thanks AT). To change the download server,
simply open `System > Administration > Software Sources` and select
download from other. Pick a random server in your country and hope for
the best (the pick the best server button does not find the least bogged
down server, it finds the closest server). The Ubuntu teem should make
this happen automatically. The System Updater should find the mirror
with the greatest download speed and use that server for its upgrade.

