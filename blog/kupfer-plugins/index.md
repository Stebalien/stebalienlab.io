---
title: "Kupfer Plugins"
date: "2010-04-25"
permalink: posts/2010/04/kupfer-plugins/
tags:
 - Kupfer
 - Extension
---

After getting frustrated with GNOME-Do's memory hogging, I switched to
[Kupfer][kupfer]. Kupfer is a lightweight, extensible application launcher like
Do (as it is now called) but much more powerful and easier to extend (it is
written in python).

## My Plugins

So far, I've written the following plugins:

1. gwibber\_plugin.py: This plugin allows you to send messages from
Kupfer through Gwibber (with no configuration).

2. exaile\_plugin.py: This plugin allows you to pause, play, skip, and
go backwards in Exaile. It is based on the Rhytmbox plugin.



+1: evolution\_plugin.py: I did not write this plugin (although I did do
a fair bit of editing). The Evolution plugin adds an evolution contact
source (and works with the built in email plugin).



Download:
[kupfer-plugins.tar.gz](./static/kupfer-plugins.tar.gz)

[kupfer]: http://engla.github.io/kupfer/
