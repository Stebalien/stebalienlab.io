---
title: "Mumbles with gmail"
date: 2008-11-16
---

I have been playing around with [mumbles](https://github.com/xiongchiamiov/mumbles)
lately and have written a gmail plugin for anyone interested. Mumbles is a
notification system that works very much like growl for the mac. The latest SVN
version can even replace the gnome notification daemon (but not very well).
While still in its infancy, this project shows a lot of promise. I based the
plugin on the evolution tutorial on project's homepage. I based the script on
[this](https://g33k.wordpress.com/2009/02/04/check-gmail-the-python-way/) script
(with many modifications). When you click on the notification popup, evolution
will launch (you can edit the source to change the program). The script
requires [python-feedparser](apt:python-feedparser).

Plugin, Source, and Script package:
[gmail-plugin\_mumbles.tar.gz](./static/gmail-plugin_mumbles.tar.gz)

Installing the precompiled plugin:


> Do step 2 from the next section. Now copy the plugin to
> `~/.mumbles/plugins/`.

Installing the plugin from source (adapted from dot\_j's
~~[tutorial](http://www.mumbles-project.org/2007/09/03/tutorial-write-an-evolution-plugin/)~~ (gone)):


> **1. Download the source**

> Download the package and extract. Then open a terminal window inside
> the plugin-source folder.

> 

> **2. Create the directory structure and choose an icon**

> Run the following command:

> ```bash
> mkdir -p ~/.mumbles/plugins/icons 
> ```

> Then either run this to use the default evolution icon...

> ```bash
> cp /usr/share/icons/hicolor/22×22/apps/evolution.png ~/.mumbles/plugins/icons 
> ```

> Or just copy your desired icon into the plugins/icons folder and
> rename it to `evolution.png`. This is unnecessary if you already have
> the evolution plugin.

> 

> **Build and install the plugin**

> ```bash
> cd ~/gmail_plugin
> python setup.py bdist_egg
> ```
>
> This will use our `setup.py` file to create our plugin. After it runs, you
> should see a file named `GmailMumbles-0.1-py2.5.egg` in the dist directory
> that was created by the build process. Here the `-py2.5` part of the filename
> refers to what version of python you are using (it may vary, but if you are
> able to start mumbles, should not matter).

> The .egg file is our plugin, so the only thing left to do is copy that
> to the mumbles plugin directory.

>
> ```bash
> cp dist/GmailMumbles-0.1-py2.5.egg ~/.mumbles/plugins
> ```

Installing the script:


> Find the script in the package and edit the username and password
> variables. Run this script with `python /path/to/gmail.py` (replace
> /path/to with the path to the script). By default, this script will
> check for new email every 2 minutes and alert the user of new mail.



BTW: This script does not safely store the username and password. I may,
in the distant future, make this script work with gnome-keyring but this
is not likely. I hope that mumbles will soon get better
notification-daemon support so that I can go back to using
[mail-notification](apt:mail-notification).

