---
title: "Gnome-Blog debs"
date: "2008-04-19"
permalink: posts/2008/04/gnome-blog-debs/
tags:
 - Blogging
 - Linux
 - software
---

I have just uploaded the patched gnome-blog deb and an updated
python-gdata deb (gnome-blog requires python-gdata 1.0.1 or up but gutsy
only includes 1.0). The python-gdata is unnecessary if you use hardy but
is a later version than the one provided by hardy.



My PPA:

deb http://ppa.launchpad.net/stebalien/ubuntu gutsy main

deb-src http://ppa.launchpad.net/stebalien/ubuntu gutsy main

Edit: I got the patch from
http://bugzilla.gnome.org/show\_bug.cgi?id=151291 (thanks to Richard
Schwarting)

