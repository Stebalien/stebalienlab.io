---
title: "Turn on keyboard led when receiving mail"
date: "2008-03-25"
permalink: posts/2008/03/turn-on-keyboard-led-when-receiving/
tags:
 - Tip
 - Linux
---

After a lot of fruitless searching I finally figured out the command to
**turn the scroll lock led on and off** in X. I first tried
`setleds +scroll` and `setleds -scroll` but that only works in a Virtual
Terminal. I then tried `xset led on` and `xset led off`. That one messes
up the num pad. I finally came up with the following:


On:

    xset led 3


Off:

    xset -led 3






I use [mail-notification](apt:mail-notification) and set the scroll lock
led to go on when I have new mail and set it to go off when I have read
the mail.

