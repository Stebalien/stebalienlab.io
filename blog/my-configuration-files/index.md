---
title: "My Configuration Files"
date: "2010-08-22"
permalink: posts/2010/08/my-configuration-files/
tags:
 - config
 - zsh 
 - openbox 
 - awesome
 - tmux
 - mutt
 - irssi

---

I have finally got around to making a git repository for my
configuration files.

Here they are:
[http://github.com/Stebalien/dotfiles](http://github.com/Stebalien/dotfiles)
