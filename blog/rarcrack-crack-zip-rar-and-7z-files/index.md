---
title: "Rarcrack: Crack zip, rar, and 7z files"
date: "2008-05-13"
permalink: posts/2008/05/rarcrack-crack-zip-rar-and-7z-files/
tags:
 - Linux
 - software
---

I have just tested [rarcrack](http://rarcrack.sourceforge.net/) and love
it. Rarcrack cracks password protected rar, 7z, and zip archives. Sadly,
it's not in the Ubuntu repository. I have therefor compiled a deb:
[rarcrack.deb](./static/rarcrack.deb). I
have not included this deb in my PPA because I compiled it with
[debianpackagemaker](http://code.google.com/p/debianpackagemaker/).

