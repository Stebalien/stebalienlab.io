---
title: "Tip: Pause a command line program"
date: "2008-04-28"
permalink: posts/2008/04/tip-pause-command-line-program/
tags:
 - Tip
 - Linux
---

This trick is known by many but it can't hurt to mention it once more. I
sometimes find myself in the middle of running a program in the command
line but would like to check on something before I continue. The easiest
way to pause a program in the command line is to hit `Control+Z`. To
restart the command simply type `fg`. Do not close the terminal window
or restart your computer because the paused program will close.

