---
title: "Screenshot of Arch"
date: "2010-04-25"
permalink: posts/2010/04/screenshot-of-arch/
tags:
 - Screenshots
 - Arch
---

I have been trying Arch Linux in VirtualBox and will probably switch
when I get around to it (or at least duel boot along with Ubuntu).

Here is what it looks like so far; if you have any questions about
tools, configs, etc., ask and I will post.

[![](./static/screenshot.th.png)](./static/screenshot.png)

