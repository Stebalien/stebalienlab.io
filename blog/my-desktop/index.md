---
title: "My Desktop"
date: "2008-03-18"
permalink: posts/2008/03/my-desktop/
tags:
 - Linux

---

After a lot of tweaking, my desktop is beginning to look how I would
like it to look.

[![](./static/screenshot.th.jpg)](./static/screenshot.jpg)


