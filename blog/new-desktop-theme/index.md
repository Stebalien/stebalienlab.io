---
title: "New Desktop Theme"
date: "2009-09-24"
permalink: posts/2009/09/new-desktop-theme/
tags:
 - Linux
 - Screenshots
---

Just in case anyone is interested, here is my new desktop layout.

[![Screenshot](./static/screenshot.th.png)](./static/screenshot.png)




**This theme works with 1280x800 screens.** (If you have a different
size screen, you will have to modify it)

Download:
[theme.tar.gz](./static/theme.tar.gz)



**Font:** [Droid Sans](apt:ttf-droid)



**Panel:** [tint2](http://code.google.com/p/tint2/)


-   Install from [this](https://launchpad.net/%7Ekilleroid/+archive/ppa)
    PPA
-   the tint2rc file (from my theme package) to "\~/.config/tint2/"

**Background:** custom (includes panel and conky backgrounds)


-   Either use my background (background.png) or replace the background
    layer in background.xcf with your own image and save it as a png.



**Conky:**


System Information and Calendar.

You can find them in the theme package.

To install:

-   Create a new folder "\~/.conky/"

-   Copy "cal.conkyrc", "cal.py" and "system.conkyrc" to "\~/.conky/"
-   Add a new startup item
    `sh -c "conky -d -c ~/.conky/system.conkyrc; conky -d -c ~/.conky/cal.conkyrc"`

Theme:

-   Metacity: Nooto
-   GTK2: Ghostship
-   Icons: Elementary

