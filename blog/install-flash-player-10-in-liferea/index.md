---
title: "Install Flash Player 10 in liferea"
date: "2008-05-21"
permalink: posts/2008/05/install-flash-player-10-in-liferea/
tags:
 - Tutorial
 - Linux
 - software
---

Installing flash player 10 in Ubuntu does not install it for
[Liferea](apt:liferea)




The fix for this is very simple:

1. First install Flash Player 10 using this
[Tutorial](https://web.archive.org/web/20100330083137/http://www.ubuntu-unleashed.com/2008/05/howto-install-flash-player-10-astro.html)

2. Next execute this command in the terminal:

    sudo ln -s /usr/lib/firefox-addons/plugins/libflashplayer.so /usr/lib/xulrunner-addons/plugins/libflashplayer.soFlash Player 10 should now work in Liferea`

