---
title: "How to record one's linux computer with pulseaudio"
date: "2009-03-21"
permalink: posts/2009/03/howto-ones-linux-computer-with/
tags:
 - Tutorial
 - identi.ca
 - Linux
---

I posted a simple tip to an Identi.ca user (mxc) explaining how to
record a skype conversation and felt that others might find the
information useful. Here is an elaborated explanation of how to record
the sound from a linux computer.


1.  Install [pavucontrol](apt:pavucontrol) and the gnome-sound-recorder
    (in the [gnome-media](apt:gnome-media) package).
2.  Open the gnome-sound-recorder and start recording
3.  Open the pulseaudio volume control and switch to the recording tab
4.  Click on the down arrow of the "gnome-sound-recorder" Record Stream,
    Select "Move Stream" and move the stream to the "Monitor" stream for
    your sound card.

This should record all sound from your computer if you are using
pulseaudio. I have not tested this with skype but it should work.

