---
title: "Bash completion with aptitude aliases"
date: "2009-05-24"
---

## Problem

1.  If I used my alias for installing an application
    (`inst application`), I would have to know the full application name
    because I would not have bash completion. This was very annoying
    when installing libraries because they often have weird version
    strings tacked on to their ends.
2.  If I used the full command (`sudo aptitude install application`), I
    would have bash completion and would therefore not have to know the
    whole application name. I could simply type `libxul` and get
    `libxul0d`.



On one hand, I would type a short command plus a complected application
name, on the other I would type a long command and a simple application
name. I wanted to be able to type a short command with a simple
application name.

## Solution

I wrote my own bash completion rules. They are based on the default aptitude
bash completion rules but customized for aliases.

```bash
# Install Completion
#
_aptitude_all()
{
        local cur dashoptions
 
        COMPREPLY=()
        cur=`_get_cword`
 
        dashoptions='-S -u -i -h --help --version -s --simulate -d \
                     --download-only -P --prompt -y --assume-yes -F \
                     --display-format -O --sort -w --width -f -r -g \
                     --with-recommends --with-suggests -R -G \
                     --without-recommends --without-suggests -t \
                     --target-release -V --show-versions -D --show-deps\
                     -Z -v --verbose --purge-unused'
       
        if [[ "$cur" == -* ]]; then
            COMPREPLY=( $( compgen -W "$dashoptions" -- $cur ) )
        else
                COMPREPLY=( $( apt-cache pkgnames $cur 2> /dev/null ) )
        fi
        return 0
}
_aptitude_installed()
{
        local cur dashoptions
 
        COMPREPLY=()
        cur=`_get_cword`
 
        dashoptions='-S -u -i -h --help --version -s --simulate -d \
                     --download-only -P --prompt -y --assume-yes -F \
                     --display-format -O --sort -w --width -f -r -g \
                     --with-recommends --with-suggests -R -G \
                     --without-recommends --without-suggests -t \
                     --target-release -V --show-versions -D --show-deps\
                     -Z -v --verbose --purge-unused'
       
        if [[ "$cur" == -* ]]; then
            COMPREPLY=( $( compgen -W "$dashoptions" -- $cur ) )
        else
                COMPREPLY=( $( _comp_dpkg_installed_packages $cur ) )
        fi
        return 0
}
complete -F _aptitude_all $default inst
complete -F _aptitude_all $default upgrade
complete -F _aptitude_all $default apt-info
complete -F _aptitude_all $default apt-changes
complete -F _aptitude_all $default apt-download
complete -F _aptitude_installed $default uninst
complete -F _aptitude_installed $default reinst
complete -F _aptitude_installed $default purge
```

Just copy it into a file such as `~/.bash_completion` and source the file in
your \~/.bashrc by adding "`. ~/.bash_completion`".

Change/Add/Remove the aliases at the end of the file. The lines that
start with `complete -F _aptitude_all` complete any available or
installed package and lines that start with
`complete -F _aptitude_installed` complete only installed packages.

inst, upgrade, apt-info, apt-changes.... are my aliases. You must use
YOUR ALIASES for this to work. To add aliases, read
[this](http://blogs.techrepublic.com.com/10things/?p=352).

