---
title: January 2014 Screenshots
date: '2014-01-31'
tags: ['Screenshots', 'Arch', 'Linux']
---

Yet another screenshot. This time with [bspwm](https://github.com/baskerville/bspwm)
and [bar](http://github.com/LemonBoy/bar) (rendered with [overkill](https://github.com/overkill)).

[![Screenshot](./static/screenshot1.th.png)](./static/screenshot1.png)


