---
title: "Grdc - A very good graphical VNC viewer for the average linux user"
date: "2009-05-20"
permalink: posts/2009/05/grdc-very-good-graphical-vnc-viewer-for/
tags:
 - Review
 - Linux
 - software
---

Overview
--------

After finding myself frustrated with
[Vinagre](http://projects.gnome.org/vinagre/)'s (GNOME's standard VNC
viewer) lack of ssh tunneling support I began looking for an
alternative. I finally found it: [Grdc](http://grdc.sourceforge.net/).



Grdc supports both VNC and RDP. Grdc can tunnel both connections over
ssh for security. The ssh tunneling supports key based or password based
authentication and allows the user to select a custom server and port
(if they differ from the defaults).



Grdc's user interface is much simpler than that of Vinagre. It allows
the user to create and save custom connections and organize them by
groups. Its connection quality settings are, unlike many remote desktop
programs, quite simple. The user simply chooses the quality and the
number of colors. Grdc also has a panel applet for the gnome-panel that
lets users connect to their saved connections through a simple menu.



Another interesting feature is the "VNC Incoming Connection" protocol.
This protocol lets users create new VNC servers with custom ports and
passwords on the fly. This is especially useful if a user wants to let a
remote user temporarily control their desktop.



Grdc does not support everything. It does not support tabbed connections
like Vinagre but, in my opinion, tabbed connections are overkill for
most users. It also does not support browsing a network for VNC
connections with avahi as Vinagre does. This feature would be useful for
administrators but is somewhat useless for the average user because most
users will access the computer from the internet, not the LAN, and thus
avahi support would be pointless.



Overall, Grdc is good for remote, internet traversing connections
because of its ssh support and is useful to the average user because of
its simplicity. Vinagre is still a better VNC client for administrators
that have to manage many computers over a LAN because, on a LAN,
encryption is usually unnecessary while avahi support is helpful and,
when managing multiple computers, tabs are very helpful.



For screenshots see the [SourceForge Screenshot
Page](http://sourceforge.net/projects/grdc/#screenshots)


Installation
------------

As of the writing of this post, the version of Grdc in the ubuntu
repositories is woefully out of date and does not support ssh tunneling.
Therefore one should use the deb provided by the project maintainers on
sourceforge. Simply download the grdc deb (link below) and install
(usually just double click). The grdc-gnome package is the gnome-panel
applet and the grdc package is the main program.



Downloads:
[http://sourceforge.net/project/showfiles.php?group\_id=248852](http://sourceforge.net/project/showfiles.php?group_id=248852)

Homepage: [http://grdc.sourceforge.net/](http://grdc.sourceforge.net/)



-- Edit: I have packaged a 64bit version. Download
[here](./static/grdc_0.5.1%7Eppa1_amd64.deb).

-- Edit 2: The 64bit version of grdc-gnome (the panel applet) for karmic
seems to work in jaunty. Download
[here](http://packages.ubuntu.com/karmic/grdc-gnome).

