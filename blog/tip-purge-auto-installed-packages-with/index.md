---
title: "Tip: Purge Auto-installed packages with Aptitude"
date: "2008-05-16"
permalink: posts/2008/05/tip-purge-auto-installed-packages-with/
tags:
 - Tutorial
 - Tip
 - Linux
---

When switching from apt-get to aptitude, I discovered that aptitude,
unlike apt-get, does not purge configuration files of auto-removed
packages when given the command `aptitude purge package`. The fix for
this is very simple.



 In order to purge configuration files **only** when the purge command
is given you must either specify the `--purge-unused` option in the
command or create an alias. To create an alias add this
`alias command='sudo aptitude purge --purge-unused'` to `~/.bashrc`.
*Command* is the command that you would like to assign for purging the
packages.



 To **always** purge configuration files for auto-removed packages add
`Aptitude::Purge-Unused=true` to `/etc/apt/apt.conf.d/05aptitude`. This
will cause aptitude to always purge configuration files for **all**
auto-removed packages regardless of weather or not the command was
`aptitude purge` or `aptitude remove`.

