---
title: "Xubuntu 8.04 Beta 1"
date: "2008-04-03"
permalink: posts/2008/04/xubuntu-804-beta-1/
tags:
 - Linux
---

I just tried out Xubuntu hardy beta on with full disk encryption.

Verdict: Broken.

1. Xubuntu would not properly display my battery percentage (the panel
applet would display 28%, 100%, or 0%).

2. The wireless worked fine and then suddenly stopped working (I had not
updated or otherwise messed with my system). When I conncted to a
wireless network said network would go down (tested with 2wire only).

I am now trying ubuntu 8.04 beta x86\_64 (the 64 bit version) on my dual
core, 64bit Intel laptop. I will post the results.

