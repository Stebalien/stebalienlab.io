---
title: "Gnome-Blog (patched)"
date: "2008-04-18"
permalink: posts/2008/04/gnome-blog-patched/
tags:
 - Tip
 - Blogging
 - Linux
---

I have just patched gnome-blog (and spent hours tweaking it) and it now
correctly posts titles with blogger. I will post the install
instructions along with the gutsy packages required tomorrow.

P.S.

I am posting this from gnome-blog.

