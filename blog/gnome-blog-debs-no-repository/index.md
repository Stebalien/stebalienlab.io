---
title: "Gnome-Blog debs (no repository)"
date: "2008-04-19"
permalink: posts/2008/04/gnome-blog-debs-no-repository/
tags:
 - Blogging
 - Linux
 - software
---

Here is the link to the deb if you don't want to add my repository.



[gnome-blog](https://launchpad.net/~stebalien/+archive/ubuntu/ppa/+files/gnome-blog_0.9.1-3ubuntu2%7Eppa3_all.deb)

