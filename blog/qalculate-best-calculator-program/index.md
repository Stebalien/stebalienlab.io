---
title: "Qalculate: The best calculator program"
date: "2008-05-25"
permalink: posts/2008/05/qalculate-best-calculator-program/
tags:
 - Review
---

I finaly found the (almost) perfect calculator program,
[qalculate](apt:qalculate-gtk). It can solve/factor equations, work with
significant figures, work with trigonometry, and much more.

Screenshots:

[![History](./static/qalculatehistory.th.png)](./static/qalculatehistory.png)
[![Keypad](./static/qalculatekeypad.th.png)](./static/qalculatekeypad.png)

