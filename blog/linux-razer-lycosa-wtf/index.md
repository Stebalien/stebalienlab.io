---
title: "Linux + Razer Lycosa = WTF"
date: "2008-09-06"
permalink: posts/2008/09/linux-razer-lycosa-wtf/
tags:
 - Review
 - Linux
---

I broke my previous keyboard and went shopping for a new one (FYI: water + G15 == not good).
I wanted another back-lit keyboard (like my previous G15). When I went to
bestbuy (not my favorite store) I saw the slick Raser Lycosa Keyboard I decided
to try it. My advice: don't. While the keyboard itself is great (I like the low,
rubber covered keys) the neither the the USB port nor the headphone jack worked
on either of the keyboards that I tried (I returned the first one and tried the
second one in the store). The problem: Interference. My computer refused to
mount my USB flash drive while plugged into the Lycosa and the headphones buzzed
(like a radio). I ended up getting the latest (annoyingly orange) G15 keyboard
which works but I HATE ORANGE.

