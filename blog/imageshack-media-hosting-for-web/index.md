---
title: "Imageshack: Media hosting for the web"
date: "2008-04-23"
---

I have seen [imageshack](http://imageshack.us/) used a lot but have
never tried it myself until now.


## Problem

Bloggers image hosting is limited (100mb) and does not work with
[lightbox](http://www.lokeshdhakar.com/projects/lightbox2/).


## Solution

Imageshack works great with blogger and lightbox. It allows for easy
posting of small thumbnails that link to the bigger image. Normally the
thumbnail would link to a web page containing the full version of the
image but this is not compatible with lightbox (lightbox needs the
actual image). The fix is very easy: Simply change the image link to the
thumbnail link but remove the `.th` from in front of the extension.

My ~~main~~ complaint is that the thumbnails are too small. I could get
around this by using the full image and scaling it down with css but
this would wast some of my 300mb per image per hour bandwidth (not
really a problem now but may become problematic with more readers).

*Edit: My __main__ complaint is that they loose images. Imageshack (along with
pretty much every other media hosting site) is great for forum posts, but not
for semi-permanent blogs.*

