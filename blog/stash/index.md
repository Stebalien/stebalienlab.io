---
title: Stash
date: 2016-09-13
cc: projects/stash
---

Stash is a library for efficiently storing maps of keys to values when one
doesn't care what the keys are but wants blazing fast `O(1)` insertions,
deletions, and lookups.

Use cases include file descriptor tables, session tables, or MIO context tables.

See the [project page](projects/stash/) for more.
