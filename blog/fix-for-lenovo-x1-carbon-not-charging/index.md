---
title: Fix For Lenovo X1 Carbon Not Charging
date: 2018-05-07
---

I recently made the mistake of plugging my Lenovo X1 Carbon (Gen 5) into a 5
volt USB-C charger overnight. The LED on the side indicated that it was charging
but, when I woke up in the morning, it obviously hadn't. Worse, it now refused
to charge even when plugged into the correct charger.

The fix is simple (although undocumented as far as I can tell). Basically, you
need to reset the battery as follows:

1. Unplug from any power sources (this won't work if you don't do this).
2. Reboot into the BIOS setup (F1 on boot).
3. Navigate to the Power menu.
4. Select the "Disable built-in battery" option.
5. Wait for the laptop to power off and then wait 30 seconds.
6. Connect the power and start the laptop.

This will temporarily disable the battery which seems to reset any "bad charger"
bits.

Hopefully, this will save others some time and frustration.

---

The X1 Carbon is otherwise a great laptop with an awesome keyboard. However,
because this is *my* blog and I can rant all I want here:

1. The dedicated Ethernet port is pretty much useless given the ubiquity of
   USB-C. I'd have much preferred an additional USB or USB-C port.
2. The nipple seems a bit firmer than the one on my X220 and also seems to get
   into the "wandering cursor" state a bit more frequently.
