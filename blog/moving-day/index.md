---
title: "Moving Day"
date: "2008-04-17"
permalink: posts/2008/04/moving-day/
tags:
 - announcement
---

I just moved to blogger (obviously) because it has comments and many
more features than Tumblr. (This move was recommended by [Josh
Stroud](http://nostalgiaofthemind.wordpress.com/))

