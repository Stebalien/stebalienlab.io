---
title: May 2013 Screenshots
date: '2013-06-02'
tags: ['Screenshots', 'Arch', 'Linux']
---

New blog, new month, new screenshots.

[![Screenshot 1](./static/screenshot1.th.png)](./static/screenshot1.png)
[![Screenshot 2](./static/screenshot2.th.png)](./static/screenshot2.png)


