---
title: "Make Firefox 3's autocomplete bar work with a dark theme"
date: 2008-05-03
---

I use MurrinaAngustifolium as my gtk theme but, because it is a dark
theme, it does not always play nicely with other applications. One
problem that I have finally fixed is the drop-down auto-complete menu
for the location bar. Put the following code in your userChrome.css file
(or use stylish).




```css
@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);
.ac-comment {
  font-size: 100% !important;
  color: #dddddd !important;
}
.ac-comment[selected="true"] {
  color: #56aaff !important;
}

.ac-url-text {
  font-size: 100% !important;
  color: #555555 !important;
}

.ac-url-text[selected="true"] {
  color: #666666 !important;
}
.autocomplete-richlistbox {
  background: #1a1a1a !important;
}
.autocomplete-richlistitem[selected="true"] {
  background: #000000 !important;
}
tooltip {
  background-color: #1a1a1a !important;
  color: #ffffff !important;
}
```




This code also makes some of the tooltips black.

