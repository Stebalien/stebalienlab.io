---
title: DRACL (thesis)
date: 2017-11-10
---

So, I never actually posted this and I figure someone out there may be
interested... For my masters thesis, I designed (but never got a chance to
implement) a decentralized (well, federated), privacy preserving, access control
protocol. The purpose of this post is *not* to explain DRACL but to get you
interested enough to download my thesis and take a quick [look][thesis].

The primary contributions are:

First, an exploration of the privacy, security, usability, reliability, and
performance trade offs involved in designing an such an access control protocol.
We explore topics like,

1. Account recovery versus security: no completely trusted third parties.
2. Privacy versus efficiency: e.g., choosing to not update something reveals
   that it hasn't changed.
3. Security versus efficiency: cached credentials, etc.
4. Ease of integration: no new services, no server-side network requests, no
   account management, etc.
5. Ease of use: groups, no surprises, etc.

etc.

Second, an interesting
(not-yet-peer-reviewed-but-probably-mostly-correct-and-definitely-interesting)
zero-knowledge (ish) set intersection protocol. We use this protocol to
construct "keys" and "ACLs" such that the keys opaquely encode the set groups in
which the user is a member and the ACLs opaquely encode the set of groups that
have access to a particular piece of content. By opaquely, I mean that neither
party learns anything about the sets that their keys encode (except what they
learn through running the protocol, see below).

Given a key that intersects with an ACL, a user can prove that the key
intersects:

1. Without either party learning anything other than the cardinality of the
   intersection (the user/prover learns this).
2. Without revealing any user/prover-identifying information to the
   website/challenger *other* than the fact that the user's key intersects with
   the ACL being proven against.

Furthermore, we can expire keys after a period of time (and even allow
semi-trusted third parties to "renew" these keys without granting these parties
access to the protected content).

There are quite a few interesting features/guarantees that are hard to sum up
succinctly so I recommend you try skimming the thesis. It goes through a lot of
trouble to try to explain DRACL in an approachable manner.

The primary downsides are:

1. Complexity. We designed the system to be simple from a usability standpoint
   but, under the covers, the actual logic, protocol, and crypto is complex.
2. Size limitations. No user in this system can feasibly have more than 1000 or
   so friends/groups of friends at a time.
3. Expensive crypto. Not "expensive crypto" from a cryptographers
   standpoint but expensive crypto from a systems engineers standpoint.
   Authenticating can take multiple seconds of CPU time.
   
[thesis]: ./static/thesis.pdf
