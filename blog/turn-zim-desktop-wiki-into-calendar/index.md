---
title: "Turn the Zim Desktop Wiki into a calendar"
date: "2010-05-18"
---

## Why

I recently switched to Arch Linux and decided to ditch evolution (a good
but bloated program). [Claws Mail](http://www.claws-mail.org/) works
perfectly as an email manager but I couldn't get its calendar plugin to
work properly. I have been using [Zim](http://zim-wiki.org/) for a while
and noticed that it had a very basic calendar plugin; this was exactly
what I needed. The plugin allows users to create pages in their wikis
for individual days: no complex forms to fill out, just a simple page to
keep track of what you are doing on a given day.


## What

As Zim lacks desktop integration so I wrote two python scripts for conky
integration:


1.  zim-conky\_cal.py

    Prints a calendar (like the cal command) with the current date and
    appointments highlighted.
2.  zim-conky\_events.py

    Lists the next 5 events or all of the events in the current month
    and the next, whichever comes first.

[![](./static/screenshot1.th.png)](./static/screenshot1.png)



I also wrote a program for adding events to the calendar (zim-cal.py and
zim-cal.ui). Select some text, run the program and double click the date
to add your text to calendar. You can also input your own text by
clicking the edit button (the big button on the right).

[![](./static/screenshot2.th.png)](./static/screenshot2.png)


## How

First: Enable the calendar plugin
(Edit-\>Preferences-\>Plugins-\>Calendar).

Download: [zimcal.tar.bz2](./static/zimcal.tar.bz2)



Conky scripts

-   Edit CAL\_PATH to point to the folder that stores your Zim calendar.
-   Add `${execpi 300 /path/to/zim-conky_cal.py}` and
    `${execpi 300 /path/to/zim-conky_events.py}` to your conkyrc

Zim-Cal program

-   Edit CAL\_PATH to point to the folder that stores your Zim calendar.
-   Make PROG\_PATH point to the directory where you put "zim-cal.ui"
-   If you intend to use this program regularly, you should probably
    assign a global hotkey to it in your window manager.

