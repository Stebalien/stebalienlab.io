---
title: Disabling Firefox Addon Signature Verification
date: 2016-08-07
---

As of Firefox 48, it's impossible to disable mandatory addon signature
verification without monkey patching Firefox or recompiling with
`MOZ_REQUIRE_SIGNING` unset. Personally, I find this unacceptable as the user
should *always* be in charge. It's also completely useless as any party powerful
enough to disable the signature verification in `about:config` could just as
easily sideload a powerful (but signed) extension like greasemonkey and then
install a malicious greasemonkey script.

Rants aside, the correct solution (for the user) is to either recompile Firefox
with mandatory signature verification disabled or use the Firefox Developer
build. Unfortunately, Firefox is a monster and recompiling it just isn't a
viable option for me (or anyone with a laptop). Also unfortunately, prepackaged
Firefox binaries are missing some *useful* security features like PIE and
dynamic libraries. Finally, the Firefox Developer build is a bit too bleeding
edge for my taste (I would like my primary browser to be relatively bug free).

So, I've written a (disgusting) script that monkey patches Firefox's `omni.ja`
to make signature verification optional again. I've only tested it on Arch Linux
but it should work on all unix-like systems. However, if your `omni.ja` file is
not in `/usr/lib/firefox/`, you'll have to tell the script where to find it
(i.e., `./nosign.sh /path/to/omni.ja`).

  * Requirements: coreutils, gawk, python2, sudo
  * Download: [nosign.sh](./static/nosign.sh) ([signature](./static/nosign.sh.sig))
  
---

**NOTE:** This script does *not* disable addon signature verification, only
makes it optional. To turn it off, you still need to set
`xpinstall.signatures.required` to false in `about:config`.

**WARNING:** This script updates the `omni.ja` file IN PLACE (using sudo).

**WARNING:** Use at your own risk.
