#!/bin/bash
##################################################
# Created By Steven Allen <Steven@stebalien.com> #
# WEB: http://stebalien.com/                     #
##################################################

file="$*"
case \
	$(zenity \
		--list \
		--title="PDF Tools" --text="Select tool" \
		--radiolist \
		--column=" " --column=Tool \
		--width="215" --height="330" \
		0 "Info" 1 "Font Info" 2 "Optimize PDF" 3 "Extract Images" \
		4 "Convert to HTML" 5 "Convert to Text" 6 "Convert to PPM" 7 "Convert to PS" \
	) in
	Info)
		pdfinfo "$file" \
		| zenity \
			--text-info \
			--width="500" --height="400" \
			--title="PDF Info"
		;;
	"Font Info")
		pdffonts "$file" \
		| zenity \
			--text-info \
			--width="675" --height="430" \
			--title="PDF Font Info"
		;;
	"Optimize PDF")
		pdfopt "$file" \
			$(zenity \
				--title="Save Optimized PDF to..." \
				--file-selection --save --confirm-overwrite \
			)
		;;
	"Extract Images")
		pdfimages "$file" \
			$(basename "$file" .pdf)
		;;
	"Convert to HTML")
		pdftohtml "$file" \
			$(zenity \
				--title="Save HTML file to..." \
				--file-selection --save --confirm-overwrite \
			)
		;;
	"Convert to Text")
		pdftotext "$file" \
			$(zenity \
				--title="Save Text file to..." \
				--file-selection --save --confirm-overwrite \
			)
		;;
	"Convert to PPM")
		pdftoppm "$file" \
			$(zenity \
				--title="Save PPM image to..." \
				--file-selection --save --confirm-overwrite \
			)
		;;
	"Convert to PS")
		pdftops "$file" \
			$(zenity \
				--title="Save PS file to..." \
				--file-selection --save --confirm-overwrite \
			)
		;;
	"")
		echo "No selection made"
		;;
	*)
		echo "ERROR: Invalid Choice"
		exit 1
		;;
esac
exit 0

