---
title: "PDF tools"
date: "2008-04-19"
permalink: posts/2008/04/pdf-tools/
tags:
 - Linux
 - software
---

A while back I wrote a script that can convert PDFs into several other
formats. It can also print information about PDFs. I recommend you use
this script in conjunction with
[nautilus-actions](apt:nautilus-actions).


## Usage

    ./PDFtools.sh [pdf document]

## Screenshots

Main Window:

[![](./static/pdftools.th.png)](./static/pdftools.png)

Info:

[![](./static/pdfinfo.th.png)](./static/pdfinfo.png)

Font Info:

[![](./static/pdffontinfo.th.png)](./static/pdffontinfo.png)

This script requires [poppler-tools](apt:poppler-tools) and [zenity](apt:zenity)

Copy Script From:
[PDFTools.sh](./static/PDFTools.sh)

