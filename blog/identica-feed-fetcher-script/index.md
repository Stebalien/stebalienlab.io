---
title: "Identi.ca feed fetcher script"
date: "2009-03-11"
permalink: posts/2009/03/identica-feed-fetcher-script/
tags:
 - Script
 - identi.ca
 - Linux
---

Attached is a simple Identi.ca feed fetcher script written in python.
It is a console program and has no gui. It will display the same
content as would appear on a users Identi.ca homepage for a given
user. Edit the configuration at the head of the script before use.


It works well with conky; simply add `${exec python /path/to/script.py}`
to your conky file and it will update whenever your conky refreshes.
**If your conky config is set to update very frequently, please use
`${execi interval /path/to/script.py}` so that you are not constantly
polling identi.ca's server.**


Edit: Updated script to correctly encode text and to include replies.
Edit2: Inline script to fix broken link.

Script:

    #!/usr/bin/python
    # -*- coding: utf-8 -*-

    # Copyright (C) 2008 Steven
    #
    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.
    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #
    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <http://www.gnu.org/licenses/>.

    # Blog: http://blog.stebalien.com

    import feedparser
    import textwrap
    import unicodedata
    ### SETTINGS ###
    # Username (lowercase)
    USER = "stebalien"
    # Width in charictars of output
    WIDTH = 50
    # Number of posts to display
    POSTS = 10

    # Indent of posts
    INDENT_FIRST = 3
    INDENT_REST = 5

    ################

    def formatPost(string):
        #string = string..replace(unicode("“", "utf-8"), '"').replace(unicode("”", "utf-8"), '"').replace(unicode("…", "utf-8"), '...').replace(unicode("’", "utf-8"), "'")
        string = unicodedata.normalize('NFKD', string).encode('ascii','ignore')
        parts = string.partition(':')
        return parts[0] + parts[1] \
        + "\n" + wrapper.fill('\n' \
        + parts[2]) \
        + "\n\n"
        
    wrapper = textwrap.TextWrapper()
    wrapper.subsequent_indent = ' '*INDENT_FIRST
    wrapper.initial_indent = ' '*INDENT_REST
    wrapper.width = WIDTH
    # Get and display feed
    all_feed = feedparser.parse("http://identi.ca/" + USER + "/all/rss")
    reply_feed = feedparser.parse("http://identi.ca/" + USER + "/replies/rss")

    n = 0
    i = 0

    while ((i+n) < POSTS):
        while (reply_feed.entries[n].updated_parsed > all_feed.entries[i].updated_parsed):
            print "> " + formatPost(reply_feed.entries[n].title)
            n=n+1
        print "  " + formatPost(all_feed.entries[i].title)
        i=i+1

