---
title: "OpenID: More accepters, less providers."
date: "2008-10-29"
permalink: posts/2008/10/openid-more-accepters-less-providers/
tags:
 - rant
---

I am noticing a very annoying and self-defeating trend with OpenIDs. The
main purpose of OpenID is to allow people to have one login for multiple
sites. OpenID is pointless if few sites accept it. This problem will
correct itself eventually as more sites adopt OpenID. The biggest
problem is the number of sites providing and not accepting OpenID. This
defeats the whole purpose of OpenID. It is pointless to have a myriad of
choices for OpenIDs if you can't use them anywhere. Multiple OpenIDs for
multiple sites just means multiple logins which is the very thing that
OpenID is supposed to fix. I hope that many sites \*ahem blogger\* will
soon start accepting OpenIDs as login credentials instead of just
providing them.

