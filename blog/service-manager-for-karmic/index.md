---
title: "Service Manager for Karmic"
date: "2009-11-01"
permalink: posts/2009/11/service-manager-for-karmic/
tags:
 - Linux
 - software
---

Annoyingly, as karmic has mostly switched to Upstart, it does not
include a service manager. While I hope that the gnome service-manager
will be updated to include support for upstart soon I have, for the
interim, written a very simple service manager.



Be warned: When I say "very simple" I mean "very simple, noob
unfriendly, and potentially dangerous". While it should not harm your
computer, I make no guarantees because it is my first PyGTK program and
was written in my spare time over a couple of days. A word of warning,
the code is very messy and inefficient (understatement).



[Link](http://gtk-apps.org/content/show.php?content=114727)

