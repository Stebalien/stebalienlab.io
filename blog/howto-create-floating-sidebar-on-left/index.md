---
title: "HOWTO: Create a floating sidebar on the left (Minima)"
date: "2008-04-22"
permalink: posts/2008/04/howto-create-floating-sidebar-on-left/
tags:
 - Tutorial
 - Tip
 - Blogging
---

It took me a while to get the floating sidebar on the left of this page
working well so I decided to write this post in order to save others
time and frustration. I made this tutorial for the Minima template but
you could modify it in order to work with any template.


Add this:

```css
/* Floating sidebar on left */
#fixedsidebar-wrapper {
  position: fixed;
  top: 75px;
  left: 10px;
  width: 100px;
  float: left;
  word-wrap: break-word; /* fix for long text breaking sidebar float in IE */
  overflow: hidden; /* fix for long non-text content breaking IE sidebar float */
}
#big-wrapper {
  margin: 0 150px 0 100px;
}
```

Below this:

```css
#sidebar-wrapper {
  width: 220px;
  float: $endSide;
  word-wrap: break-word; /* fix for long text breaking sidebar float in IE */
  overflow: hidden;     /* fix for long non-text content breaking IE sidebar float */
}
```

Then, to fix the page elements tab...

Add this:

```css
/** Page structure tweaks for layout editor wireframe for floating sidebar on left **/

body#layout #outer-wrapper,
body#layout #header-wrapper,
body#layout #footer {
  width: 500px;
  padding: 0px;
}
body#layout #main-wrapper {
  width: 300px;
}
body#layout #sidebar-wrapper {
  width: 200px;
}
body#layout #newsidebar-wrapper {
  width: 150px;
}
```

...below the previous code.

To remove the dotted line under widgets...

Add this:

```css
#fixedsidebar.sidebar .widget {
  border-bottom-width: 0 !important;
}
```

Below this:

```css
.sidebar .widget, .main .widget {
  border-bottom:1px dotted $bordercolor;
  margin:0 0 1.5em;
  padding:0 0 1.5em;
}
```

Then add `<div id="big-wrapper">` just under `<body>`. This will make
sure that the floating sidebar will not overlap the other content.

Now in order to actually place the sidebar...

Add this:

```html
      <div id="fixedsidebar-wrapper">
   <b:section class="sidebar" id="fixedsidebar" preferred="yes">
      <b:widget id="NewProfile" locked="false" title="'About" type="Profile" />
    </b:widget>
  </b:section>
</div>
```

Below this:

```html
    <div id="content-wrapper">
 <div id="crosscol-wrapper" style="">
   <b:section class="crosscol" id="crosscol" showaddelement="no" />
 </b:section>
</div>
```

Finally, add a `</div>` tag before the `</body>` tag to close the "big-wrapper"

This post is heavily based on this
[tutorial](http://tips-for-new-bloggers.blogspot.com/2007/02/three-columns-blogger-template.html)
but creates a fixed (floating) sidebar instead of a sidebar that scrolls
with the rest of the page.

