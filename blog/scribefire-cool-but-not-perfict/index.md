---
title: "Scribefire: Cool but not perfect"
date: "2008-04-20"
permalink: posts/2008/04/scribefire-cool-but-not-perfict/
tags:
 - Review
 - Blogging
---

Scribefire is a very useful extension and can even **upload images** to
blogger but has a few annoying "bugs" for lack of a better word.



Scribefire pre-release and dark-themes (gtk) don't mix well (it looks
very ugly). Scribefire current release works better with dark themes but
not perfectly.



Scribefire can upload images but does not upload them to the standard
server (it uploads them to "Picasa web albums") and, unlike the default
editor, it simply places a shrunken version of the image on the webpage
and does not link it to its bigger version. This is easily fixable but
annoying (especially when using lightbox).



Too many features and few options to trim down the UI.



Verdict: Fun but not for me (yet).

