---
title: "Banshee Plugin for mumbles"
date: "2008-11-20"
---

I Just wrote a banshee plugin for mumbles. It works much better than
banshee's default notification daemon integration. Clicking on the
notification brings banshee to the front. Install it like you would any
other mumbles plugin (see two posts back or look for instructions on the
mumbles [homepage](https://github.com/xiongchiamiov/mumbles)).

Plugin (python 2.5) + Source:
[banshee-plugin\_mumbles.tar.gz](./static/banshee-plugin_mumbles.tar.gz)
