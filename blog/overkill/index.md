---
title: Overkill Initial Release
date: 2013-11-09
cc: projects/overkill
---

I have been working on a project I call overkill for the past few months or so.
It's is a publish-subscribe framework (or a functional-reactive programming
framework if you want to use the latest buzz word) for collecting and
distributing information on a local machine. Personally, I use it to generate
my status bar (hence the name, overkill). See the [project][project] page for
more information.

[project]: projects/overkill

