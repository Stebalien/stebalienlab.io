---
title: "Write a file as root from a non-root vim."
date: "2009-08-14"
permalink: posts/2009/08/write-file-as-root-from-non-root-vim/
---

## Problem

I often edit config files in vim, try to save them, and then realize
that I ran vim as a normal user and the config file is in /etc. In the
past I would close vim and redo all of my changes as root (using sudo).
This is a real pain especially if I had made a lot of changes.


## Solution

Add the following to your \~/.vimrc: 


    cmap w!! w !sudo tee % >/dev/null<CR>:e!<CR><CR>


A while ago I came across this
[post](https://raam.org/2009/saving-files-as-root-from-inside-vim/)
that describes how to save a file as root from a non-root vim by adding
`cmap w!! w !sudo tee % >/dev/null` to my .vimrc.



The problem is that, after running this command, vim will notice that
the file has changed on disk and ask the user if he or she wants to
reload it. After a while this got annoying, therefore the multiple CRs
(enters) and the :e! (reloads the file).

