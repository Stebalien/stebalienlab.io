---
title: Gazetta
date: 2015-07-13
cc: projects/gazetta
---

Gazetta is a static site generator written in rust (currently powering this
website). You can find more details on the [project](projects/gazetta) page.
