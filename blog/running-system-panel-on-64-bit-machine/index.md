---
title: 'Running the "Ubunu System Panel" on a 64 bit machine.'
date: "2008-06-19"
permalink: posts/2008/06/running-system-panel-on-64-bit-machine/
tags:
 - Linux
---

After installing the 64 bit version of Ubuntu on my Dell Inspiron 1420n
I found that the USP (Ubuntu System Panel) keybindings do not work. This
is because the
`/usr/lib/python2.4/site-packages/usp/plugins/_keybinder.so` is compiled
for a 32 bit system. After a little searching I found out that
[Glipper](apt:glipper) includes a `_keybinder.so` file. After replacing
USP's \_keybinder.so with Glipper's version, the keybindings worked
fine.



1. Download glipper `aptitude download glipper`.

2. Extract the glipper deb with
`dpkg-deb -x glipper_1.0-1ubuntu1_amd64.deb glipper` (change
glipper\_1.0-1ubuntu1\_amd64.deb to the name of your file).

3. Copy in the new \_keybinder.so with
`sudo cp ./glipper/usr/lib/python-support/glipper/python2.5/glipper/keybinder/_keybinder.so /usr/lib/python2.4/site-packages/usp/plugins/_keybinder.so`.

4. Restart USP with `killall gnome-panel` and your USP shortcut keys
should now work.

Edit: This is no longer necessary.


