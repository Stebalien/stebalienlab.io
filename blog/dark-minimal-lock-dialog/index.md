---
title: "Dark-Minimal Lock Dialog"
date: "2009-07-27"
permalink: posts/2009/07/dark-minimal-lock-dialog/
---

I finally uploaded my first contribution to
[gnome-look.org](http://gnome-look.org/): [Dark-Minimal Lock
Dialog](http://gnome-look.org/content/show.php?content=109151).


## Screenshot

[![Screenshot](./static/screenshot.th.png)](./static/screenshot.png)


## Download

[Gnome-Look.org](http://gnome-look.org/content/show.php?content=109151)

