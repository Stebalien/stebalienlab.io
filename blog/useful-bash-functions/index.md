---
title: "Useful Bash functions"
date: "2010-04-27"
permalink: posts/2010/04/useful-bash-functions/
tags:
 - Tutorial
 - bash
---

## cdd: cd and list the files.

```bash
function cdd(){ cd $*  ls --color}
```

## changelog: get the change log for a program

```bash
changelog() {
    log=/usr/share/doc/"$*"/changelog*
    if [ -r $log ]; then
        less $log
        unset log
    else
        log=/usr/share/doc/"$*"/CHANGELOG*
        if [ -r $log ]; then
            less $log
        fi
    fi
}
```

## mkdircd: make a directory and move in.

```bash
function mkdircd() {
  mkdir $* cd ${!#}
}
```

