---
title: "Ubuntu hardy upgrade status: Buggy but working"
date: "2008-04-25"
permalink: posts/2008/04/ubuntu-hardy-upgrade-status-problamatic/
tags:
 - Tip
 - Linux
---

I ran into a few problems during the upgrade but all of them were
fixable.



Partial Upgrade fix: Downgrade non-ubuntu packages to their ubuntu
versions if an ubuntu version exists. My system refused to complete the
upgrade (and I had to upgrade it through aptitude) because of a few
package conflicts (mostly from the schmidtke repository).



Numpad Fix: My computers numpad stopped working after upgrading. Somehow
an acceptability option had been turned on. To fix this go to
`System > Preferences > Keyboard` and go to the mouse keys tab and
uncheck the checkbox.



Remove custom G15 drivers: A while back I had installed the G15 drivers
based on a tutorial posted on the ubuntu wiki. These drivers should now
be uninstalled as Ubuntu Hardy comes with its own drivers.



Everything appears to be working fine now.

