---
title: "Mumbles screenshot"
date: "2008-11-20"
permalink: posts/2008/11/mumbles-screenshot/
tags:
 - Linux
 - Mumbles
 - Screenshots
---

Here is a screenshot of mumbles.

[![Mumbles Screenshot](./static/screenshot.th.png)](./static/screenshot.png)

