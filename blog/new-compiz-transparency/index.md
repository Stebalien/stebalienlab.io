---
title: "New Compiz transparency"
date: "2008-06-07"
permalink: posts/2008/06/new-compiz-transparency/
tags: 
 - Linux

---

This may not be new but, after upgrading compiz from git today, I
noticed that certain windows (gnome-system-monitor and the AWN manager)
were semi-transparent. See the screenshot:

[![Semi-Transparent Compiz Windows](./static/screenshot.th.png)](./static/screenshot.png)

Edit: As far as I can tell, the Murrine engine, not compiz, is not
causing the transparency.

