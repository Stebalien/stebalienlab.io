---
title: "Latte rant"
date: "2008-06-19"
permalink: posts/2008/06/latte-rant/
tags:
 - rant
---

I religiously drink decaf lattes (by "religiously" I mean every Sunday) and am
becoming very annoyed with the common misconception that lattes are just coffee
and milk. True lattes are not made with standard coffee beans, they are made
with espresso beans. Espresso beans are roasted longer and thus are much richer
but less bitter. Many so called coffee shops claim to sell lattes but use
regular coffee beans in their espresso machine. The resulting drink is not a
café latte. It is called a café au lait.


Here's my plea: Coffee shops, please correctly label your drinks.


PS: If you go to Canada, bring your own espresso. I can't find a decent
Latte in all of Nova Scotia.

