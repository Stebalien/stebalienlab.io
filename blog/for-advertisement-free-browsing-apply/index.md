---
title: "For advertisement free browsing: apply Adblock Plus"
date: "2008-04-22"
permalink: posts/2008/04/for-advertisement-free-browsing-apply/
tags:
 - Tip
 - Review
 - Extension
 - software
---

[Adblock Plus](http://adblockplus.org/) is a great Firefox extension
that blocks advertisements. Advertisements hog bandwidth and are
distracting. It is easy to use (right click and select block element).

Here is my adblock plus filter list (save it as a text document and
import it).


```text
(Adblock Plus 0.7 or higher required) [Adblock]
@@netstream
@@http://www.microsoft.com/
@@http://www.windowsmarketplace.com/
@@|http://www.kiisfm.com/
@@|http://www.apple.com/
@@|https://www.windwardschool.org/
@@|http://iwonderdesigns.com/
@@|http://news.bbc.co.uk/
@@|http://wow.weather.com/
@@|http://www.defensedevices.com/
@@http://www2.gawker.com/assets/minify.php?files=*
@@http://syndication.mmismm.com/
@@http://tags.gawker.com/
*.2o7.net/*
*/shopping_deals*
*/tacoda/*
*_fromoursponsors_*
/(\W|_)(ad(s)?(vert(is(ement|ing|er)?)?)?)(s)?(view|log|image(s)?|Links|counter|serve(r)?|sys|brite|sonar|lengend|side|files)?(\W|_|1)/
/[\W_](blog|get|online)ad(s)?[\W_]/
/[\W_](double|fast)click[\W_]/
/\.geocities.com/js_source/(ygNSLib9|pu5geo).js/
/\D(728|588|468|234|160|120)x(600?|90)\D/
http://*.sageanalyst.net/*
http://*.centrport.net/*
http://*.falkag.net/*
http://*.industrybrains.com/*
http://*.itnation.com/*
http://*.questionmarket.com/*
http://*.spinbox.net/*
http://*.tiser.com.*/
http://*.tribalfusion.com/*
http://*.valueclick.com/*
http://*theglobeandmail.com/adsv3/*
http://ehg*.hitbox.com/*
http://graphics8.nytimes.com/marketing/
http://images.pcworld.com/shared/graphics/bevel_*.gif
http://images.stardock.com/store/*
http://images.usatoday.com/kk/classified/*
http://infospace.abcnews.com/*/adinsert.js
http://media.nyadmcncserve*.com/
http://media.washingtonpost.com/wp-srv/popjs/popup*
http://msnbc.msn.com/#DIV(p2)
http://msnbc.msn.com/#DIV(textSmallGrey)
http://msnbc.msn.com/*#DIV(clr)
http://msnbc.msn.com/*#DIV(w779)
http://pagead2.googlesyndication.com/*
http://rcm.amazon.com/e/cm?t=*
http://servedby.*
http://server.*.liveperson.net/*
http://www.goodsforyou.com/*
http://www.humaxusa.com/*
http://www.insanely-great.com/*/adsb.cgi*
http://www.kiisfm.com/cc-common/CCAS_media/ccas_creative_81152.gif
/.*(-|_|/)promo(-|_).*\.(gif|png|jpg)/
http://www.qksrv.net/*
intellitxt
*/promotions/*
http://us.a1.yimg.com/*
http://download-search.search.com/search?v=og&q=*
http://i.d.com.com/*/*_sponsors_*.gif
http://www.ftjcfx.com/*
|http://*.addfreestats.com/*
/http://feeds\.(\w|\W)+\.com/~[a]/(\w|\W)+\?.=(\w|\W)+/
http://*.casalemedia.com/*
http://www.lduhtrp.net/*
http://www.afcyhf.com/*
http://www.shareasale.com/*
http://3ps.go.com/DynamicAd?*
/[\W|_](((p|P)romo)(tional)?)?(_)?(promo(s)?|banner)(s)?(_)?(up|down|standard|right|left|top|bottom)?([0-9])?\.(gif|png|jpg|bmp)/
http://*.atwola.com/*
http://ccas.clearchannel.com/*&affiliate=*
http://network.business.com/*
http://affiliates.digitalriver.com/*
http://clk.atdmt.com/*
http://www.awltovhc.com/image-*
http://*.adland.ru/*
http://www.macdailynews.com/adpeeps/*
http://rss.sourceforge.net/~a/*
http://cgi.insecure.org/cgi-bin/pro/*
http://affiliates.babylon.com/*
http://view.atdmt.com/*
/.*(_|/)sponsor(s)?((\.(gif|jpg|png))|(/))/
http://bs.serving-sys.com/BurstingPipe/BannerSource.asp?*
*/web_banners/*
*/bin/ad
http://*.google.com/pagead/*
*/images/banner*.png
http://s1.amazon.com/exec/varzea/tipbox/*
*_sweeps*
http://www.conduit.com/banners/*
*adpartner*
http://digg.com/img/feature-*.gif
http://*.freesoftwaremagazine.com/openads/*
http://regmedia.co.uk/*
http://www.siteground.com/img/banners_cust/2*
http://ubuntulinuxhelp.com/hic300250.gif
http://www.tqlkg.com/*
http://groups.google.com/groups/adfetch?*
http://banner.casinotropez.com/*
http://www.speedsuccess.net/*
#*(sponsored_links_article)
#*(ad_bottom)
#*(ad_top)
#*(launchpad-ads-3)
#*(sponsorship)
#*(sponsorshiphack)
#*(promo2)
#*(ad_300)
#*(ad_180)
#*(adbannerblock)
#*(ad34)
#*(googlesyndication)
##div.banner ad + *
google.com#TABLE(cellpadding=9)(border=0)
openssh.com#BLINK
google.com#a(href^=https://www.google.com/ads)
babelfish.yahoo.com#DIV(ovt)
cnet.com#DIV(class=asl_margin)
cnet.com#DIV(class=rb_pft_ad)
#DIV(id=spons_links)
#DIV(class=columnGroup advertisementColumnGroup)
#A(href^=http://www.servage.net/?coupon=)
flickr.com#DIV(id=AdBlock)
youtube.com#DIV(id=leaderboardAd)
google.com#TD(rowspan=5)
google.com##TD[onmouseover="return ss()"]
google.com#H2(class=sl f)
#DIV(class=sponsor)
youtube.com#NOSCRIPT(class=noscript-show)
google.com#DIV(id=tpa1)
linuxforums.org#DIV(id=topbanner)
linuxforums.org#NOSCRIPT(class=noscript-show)
linuxquestions.org#NOSCRIPT(class=noscript-show)
#DIV(id=rightad)
#DIV(id=topad)(class=ad)
#A(class=sponsoredlink)
#DIV(id=ad-top)
#DIV(class=banner ad)
opendns.com#UL(class=adkeywords)
google.com##TABLE.contentitem[cellspacing="0"][cellpadding="5"][border="0"][bgcolor="#ffffff"][style]
google.com##TD[nowrap][onmouseover="return true"]
google.com#*(href^=http://www.google.com/sponsoredlinks?)
builtwith.com#DIV(class=featuredTechnology)
flashlinux.org.uk#DIV(id=portlet-dtm)
#*(id=sponsors)
#DIV(id=ads)
techcrunch.com#A(href=/advertise/)
#A(href^=http://www.google.com/pagead)
dictionary.reference.com#A(href^=http://www.google.com/url?sa=L&ai=)
#IMG(src^=http://ad.uk.doubleclick.net)
#IMG(src^=http://ad.doubleclick.net)
yahoo.com#A(href=http://us.ard.yahoo.com/SIG=12lt27gkl/M=570179.11705344.12177319.9641256/D=yahoo_top/S=2716149:HDLN/_ylt=AvsqK7FgUbxkrtlnjbKs1lP1cSkA/Y=YAHOO/EXP=1193800089/A=5005233/R=0/SIG=14m0dtrqi/*https://www.getsmart.com/refi/qform.asp?bp=ltquickmatch&esourceid=1271910&promo=00221&num=1-866-262-6895&AdType=2&version=45&disable_popups=1&init=1)
howstuffworks.com#A(class=reify-linkifier)
#DIV(class=topbanners)
#DIV(id=holidayad)
#DIV(class=doubleClickAd)
#*(class=doubleClick)
#DIV(class=adText)
guide.opendns.com#DIV(id=noresultsads)
#DIV(class=ad_itext_bar)
search.com#UL(class=sponsored)
experts-exchange.com#DIV(class=ontopBanner)
guide.opendns.com#DIV(id=adbox)
us.f812.mail.yahoo.com#TABLE(id=welcomeAdsContainer)
books.google.com#DIV(class=ad)
weather.yahoo.com#DIV(class=yw-ad)
lifehacker.com#DIV(id=sitemeter)
#DIV(class=advertContainer)
```
