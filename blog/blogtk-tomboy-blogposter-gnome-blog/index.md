---
title: "BloGTK + tomboy-blogposter + gnome-blog"
date: "2008-04-17"
permalink: posts/2008/04/blogtk-tomboy-blogposter-gnome-blog/
tags:
 - Blogging
 - Linux
---

I have tested [BloGTK](apt:blogtk) + [tomboy-blogposter](apt:tomboy-blogposter) +
[gnome-blog](apt:gnome-blog) and all of them have problems with blogger.
Gnome-blog is the simplest. It is a panel applet for the gnome-panel and has a
very clean, simple, and fast interface. On the other hand, BloGTK is a
standalone app that is a little bulky. Neither of these programs will post
titles of posts correctly (BloGTK refuses to allow a title at all and
gnome-blog posts the title as part of the body of the post).  Tomboy-Blogposter
does post the title correctly (because it uses the new API) but does not allow
for HTML (links etc...). I am therefore resigned to look for another program (I
will try Scribfire next).



Edit: Gnome-Blog cannot post pictures to blogger.

